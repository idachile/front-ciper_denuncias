(function(window, document, $){
	"use strict";
	var DEBUG = true;
	var $window = $(window),
	$document = $(document),
	$body = $('body'),
	$mainNav, $mainHeader;

	/// guardo los media queries
	var TABLETS_DOWN = 'screen and (max-width: 64em)',
	VERTICAL_TABLETS_DOWN = 'screen and (max-width: 50em)',
	VERTICAL_TABLETS_UP = 'screen and (min-width: 50em)',
	PHABLETS_DOWN = 'screen and (max-width: 40em)';

	var throttle = function( fn ){
		return setTimeout(fn, 1);
	};

	var mqMap = function( mq ){
		var MQ = '';

		switch( mq ){
			case 'tablet-down' :
			MQ = TABLETS_DOWN;
			break;
			case 'vertical-tablet-down' :
			MQ = VERTICAL_TABLETS_DOWN;
			break;
			case 'phablet-down' :
			MQ = PHABLETS_DOWN;
			break;
		}

		return MQ;
	};

	var normalize = (function() {
		var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç",
		to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
		mapping = {};

		for(var i = 0, j = from.length; i < j; i++ )
		mapping[ from.charAt( i ) ] = to.charAt( i );

		return function( str ) {
			var ret = [];
			for( var i = 0, j = str.length; i < j; i++ ) {
				var c = str.charAt( i );
				if( mapping.hasOwnProperty( str.charAt( i ) ) )
				ret.push( mapping[ c ] );
				else
				ret.push( c );
			}
			return ret.join( '' );
		};
	})();



	String.prototype.capitalize = function() {
		return this.charAt(0).toUpperCase() + this.substring(1).toLowerCase();
	}

	var App = function(){};

	App.prototype = {
		onReady : function(){
			this.conditionalInits();
			this.autoHandleEvents( $('[data-func]') );
            this.initTextCounter();
            this.initAreaResizer();
            this.initValidFile();
            this.initInputClean();
		},

		onLoad : function(){},

		autoHandleEvents : function( $elements ){
			if( !$elements || !$elements.length ){ return false; }

			var self = this;

			$elements.each(function(i,el){
				var func = el.getAttribute('data-func') || false,
				evts = el.getAttribute('data-events') || 'click.customStuff';

				if( func && typeof( self[func] ) === 'function' ){
					$(el)
					.off(evts)
					.on(evts, $.proxy(self[func], self))
					;
				}
			});
		},

		conditionalInits : function( $context ){
			if( !$context ){
				$context = $document;
			}
			// delegaciones directas
			if( $context.find('[data-func]').length ){
				this.autoHandleEvents( $context.find('[data-func]') );
			}

			$(".sidebar-confirm").stick_in_parent();
			this.singleMenu();
		},
		singleMenu : function(event){
			//arma el menu side en el single
			$('.single__content h2').each(function( index ) {
				$( this ).attr('id', "h2_"+index);
				var texto = $( this ).text()
				var html ='<li class="sidebar__item"><a href="#h2_' + index +'" title="Ir a '+ texto.capitalize() +'" data-func="scrollToTarget" data-offset="40">'+ texto.capitalize() +'</a></li>';
				$('.sidebar__indice').append(html)
			});
		},
		filtrar_inicial : function(event){
			var $selctor = $(event.currentTarget);
			var inicial = $selctor.text();
			$selctor.siblings().removeClass("alphabet__item--current");
			$selctor.addClass("alphabet__item--current");
			$('[data-search]').val("")
			$('.simple-row--top article').hide();
			$('.simple-row--top article[data-inicial="'+inicial+'"]').show();
		},
		buscar_inicial : function(event){
			event.preventDefault();
			var s = $('[data-search]').val();
			if(s==""){
				$('.simple-row--top article').hide();
				$('.simple-row--top article[data-inicial="A"]').show();
				return;
			}

			s = s.toLowerCase();

			s  = 	s.replace(new RegExp(/\s/g),"");
			s  = 	s.replace(new RegExp(/[àáâãäå]/g),"a");
			s  = 	s.replace(new RegExp(/æ/g),"ae");
			s  = 	s.replace(new RegExp(/ç/g),"c");
			s  = 	s.replace(new RegExp(/[èéêë]/g),"e");
			s  = 	s.replace(new RegExp(/[ìíîï]/g),"i");
			s  = 	s.replace(new RegExp(/ñ/g),"n");
			s  = 	s.replace(new RegExp(/[òóôõö]/g),"o");
			s  = 	s.replace(new RegExp(/œ/g),"oe");
			s  = 	s.replace(new RegExp(/[ùúûü]/g),"u");
			s  = 	s.replace(new RegExp(/[ýÿ]/g),"y");
			s  = 	s.replace(new RegExp(/\W/g),"");

			s = s.toLowerCase().replace(/\b[a-z]/g, function(letter) {
				return letter.toUpperCase();
			});
			$('.simple-row--top article').hide();
			$(".simple-row--top article[data-fullname*='"+s+"']").show();
		},

		////////////
		//////////// Delegaciones directas
		////////////
		calendarControl : function( event ){
			event.preventDefault();

			var app = this;

			var $btn = $(event.currentTarget),
			$dataHolder = $btn.parents('[data-role="calendar-data"]'),
			$itemsHolder = $('[data-events-holder]'),
			$monthName = $('[data-role="calendar-month"]'),
			direction = $btn.data('direction'),
			month = $dataHolder.data('month'),
			year = $dataHolder.data('year'),
			filter = $dataHolder.data('filter');

			if( app.isLoading ){
				return;
			}

			app.isLoading = true;

			/// se debe indicar que se esta cargando, asumo estados estandar
			/// eso queire decir, opacity nomas
			$monthName.css('opacity', '0.2');
			$itemsHolder.css('opacity', '0.2');

			$.getJSON('/wp-json/st-rest/actividades', {
				direction : direction,
				month : month,
				year : year,
				filter : filter
			}).then(function( response ){
				$dataHolder.data('month', response.month_num);
				$dataHolder.data('year', response.year);

				$monthName
				.text( response.month_name + ' - ' +  response.year)
				.attr({
					'data-prev' : response.prev,
					'data-next' : response.next
				});

				$itemsHolder.html( response.items );

				$monthName.css('opacity', '1');
				$itemsHolder.css('opacity', '1');

				app.isLoading = false;
			});
		},

		deployTarget : function( event ){
			event.preventDefault();

			var $item = $(event.currentTarget),
			target = $item.data('target'),
			$targetElem;

			if( !target ){
				console.warn('No se especificó un objetivo en el atributo "data-target":', target );
				return;
			}

			$targetElem = $(target);
			if( !$targetElem.length ){
				console.warn('El objetivo no fue encontrado o el atributo "data-target" no es un selector válido :', target );
				return;
			}

			$item.data('animating', true);

			if( $item.data('deployed') ){
				$targetElem
				.slideUp().promise()
				.then(function(){
					$item.data({
						deployed : false,
						animating : false
					}).removeClass('deployed');

					$targetElem.removeClass('deployed');
				});
			}
			else {
				$targetElem
				.slideDown().promise()
				.then(function(){
					$item.data({
						deployed : true,
						animating : false
					}).addClass('deployed');

					$targetElem.addClass('deployed');
				});
			}
		},

		deployMainNav : function( event ){
			event.preventDefault();

			var $btn = $(event.currentTarget),
			$nav = $('[data-role="nav-container"]'),
			$headerBody = $('[data-role="header-body"]');

			if( $btn.data('deployed') ) {
				$btn
				.data('deployed', false)
				.removeClass('deployed');

				$nav.removeClass('deployed');
				$headerBody.removeClass('deployed');
			}
			else {
				$btn
				.data('deployed', true)
				.addClass('deployed');

				$nav.addClass('deployed');
				$headerBody.addClass('deployed');
			}
		},

		deployCollapsable : function( event ) {
			event.preventDefault();

			var $item = $(event.currentTarget),
			$targetElem = $item.parents('.collapsable').find('.collapsable-body');

			$item.data('animating', true);

			if( $item.data('deployed') ){
				$targetElem
				.slideUp().promise()
				.then(function(){
					$item.data({
						deployed : false,
						animating : false
					}).removeClass('deployed');

					$targetElem.removeClass('deployed');
				});
			}
			else {
				$targetElem
				.slideDown().promise()
				.then(function(){
					$item.data({
						deployed : true,
						animating : false
					}).addClass('deployed');

					$targetElem.addClass('deployed');
				});
			}
		},

		closeModal : function( event ){
			event.preventDefault();
			$('[data-module="modal"]').remove();
		},
		//data-autoresize
		initAreaResizer : function(){
			jQuery.each(jQuery('textarea[data-autoresize]'), function() {
				var offset = this.offsetHeight - this.clientHeight;
				var resizeTextarea = function(el) {
					jQuery(el).css('height', 'auto').css('height', el.scrollHeight + offset);
				};
				jQuery(this).on('keyup input', function() { resizeTextarea(this); }).removeAttr('data-autoresize');
			});
		},
		//data-role="textcounter"
		//data-role="countdown"
		initTextCounter : function(){
			$('[data-role="textcounter"]').keyup(function(e){
				var $textarea = $(this),
					maxlength = parseInt($textarea.attr('maxlength')),
					valuelength = $textarea.val().length,
					countdown = $textarea.parent().find('[data-role="countdown"]');
	
				e.preventDefault();
				countdown.text(maxlength - valuelength);

			});
		},

		initInputClean : function(){
			$('[data-role="cleanspace"]').on({
				keydown: function(e) {
					if (e.which === 32) return false;
				},
				change: function() {
					this.value = this.value.replace(/\s/g, "");
				}
			});
		},

		initValidFile: function(){
			$('[data-role="validfile"]').change(function(e){
				var $input = $(this),
					filename = $(this).val(),
					$father = $input.parents('.form-control').find('.form-control--file'),
					$mother = $input.parents('.form-control'),
					maxsize = $input.data('max-size'),
					filesize = $input.get(0).files[0].size,
					$okmessage = $mother.find('.form-control__text').text(),
					$errormessage = $mother.data('error-message');

				e.preventDefault();
				console.log('filesize: ' + filesize);
				console.log('okmessage: ' + $okmessage);
				//define el limite de tamaño
				if(maxsize){
					maxsize = maxsize;
					console.log('limit defined:' + maxsize);
				}else{
					maxsize = 100000 //100mb
					console.log('limit undefined:' + maxsize);
				}

				if(filesize>maxsize){
					console.log('too heavy');
					$mother.find('.form-control__text').html($errormessage);
					return false;
				}else{
					console.log('just right');
					$mother.find('.form-control__text').hide();
					if (filename.substring(3,11) == 'fakepath') {
						filename = filename.substring(12);
					}
					if(!filename){
						filename = $father.find('.content__btn--file__desc').text();
					}
					$father.find('.content__btn--file__desc').html(filename);
					return true;
				}
			});
		}
		
	};

	var app = new App();
	$document.ready(function(){ app.onReady && app.onReady(); });
	$window.on({'load' : function(){ app.onLoad && app.onLoad(); }
});

}(window, document, jQuery));
